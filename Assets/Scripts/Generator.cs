﻿using UnityEngine;
using System.Collections;
using System;

public class Generator : MonoBehaviour {

    void Start()
    {
        string classString = "Champion";

        object obj = CreateInstance(classString);
        FindFieldsAndFill(obj, 3f, 12.9f, 6, 12);
    }

    object CreateInstance(string _className)
    {
        Type type = Type.GetType(_className, true);
        Debug.Log("Create instance of class " + _className);
        return Activator.CreateInstance(type);  // try / catch
    }

    object CreateInstance(string _className, string _parent) // for nested classes
    {
        Type type = Type.GetType(_parent + "+" + _className, true);
        Debug.Log("Create instance of class " + _parent + "+" +_className);
        return Activator.CreateInstance(type);  
    }

    void FindFieldsAndFill(object _obj, float _minNumber, float _maxNumber, int _minStrLenght, int _maxStrLenght)
    {
        // searching for nested classes
        foreach (var prop in _obj.GetType().GetNestedTypes())
        {
            object nestedObject = CreateInstance(prop.Name, _obj.GetType().FullName);
            FindFieldsAndFill(nestedObject, _minNumber, _maxNumber, _minStrLenght, _maxStrLenght);
        }

        // searching for fields and fill it
        foreach (var field in _obj.GetType().GetFields())
        {
            if (field.FieldType == typeof(System.Single)) // float
            {
                _obj.GetType().GetField(field.Name).SetValue(_obj, UnityEngine.Random.Range(_minNumber, _maxNumber));   
            }

            if (field.FieldType == typeof(System.String)) // string
            {
                string letters = "abcdefghijklmnopqrstuvwxyz";
                int charAmount = UnityEngine.Random.Range(_minStrLenght, _maxStrLenght);
                string txt ="";
                for (int i = 0; i < charAmount; i++) txt += letters[UnityEngine.Random.Range(0, letters.Length)];
                _obj.GetType().GetField(field.Name).SetValue(_obj, txt);
            }
        }

        // print fields and values;
        PrintFields(_obj);
    }

    private void PrintFields(object _obj)
    {
        foreach (var field in _obj.GetType().GetFields())
        {
            Debug.Log(_obj.GetType().FullName + "." + field.Name + " = (" + field.FieldType.Name + ") " + field.GetValue(_obj).ToString());
        }
    }


}
    

