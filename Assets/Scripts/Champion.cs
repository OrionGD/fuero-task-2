﻿using UnityEngine;
using System.Collections;

public class Champion {

	public string championsName;
    public float weight;
    public float height;

    public class Weapon
    {
        public string nameOfWeapon;
        public float cost;
        public bool active = true;

        public class Damage
        {
            public float dealtDamage;
        }
    }

}
